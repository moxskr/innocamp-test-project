import React from 'react';

const EmptyZone = ({message}) => {
    return(
        <div className="empty-zone col-lg-12 text-center">
            <h2>{message}</h2>
        </div>
    )
};

export default EmptyZone;
