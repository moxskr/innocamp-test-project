import React from 'react';

const Search = ({changeFunc, searchValue}) => {
    return(
        <div className="search col-lg-6 d-flex justify-content-center">
            <input
                type="text"
                value={searchValue}
                onChange={changeFunc}
                placeholder="Search..."
            />
        </div>
    )
};

export default Search;
