import React from 'react';

const filterYears = [
    "1990",
    "1991",
    "1992",
    "1993",
    "1994",
    "1995",
    "1996",
    "1997",
    "1998",
    "1999",
    "2000",
    "2001",
    "2002",
    "2003",
    "2004",
    "2005",
    "2006",
    "2007",
    "2008",
    "2009",
    "2010",
    "2011",
    "2012",
    "2013",
    "2014",
    "2015",
    "2016",
    "2017",
    "2018",
    "2019",
];

const SearchYear = ({year, changeFunc}) => {

    return(
        <div className="search-year col-lg-3 d-flex justify-content-end">
            <select onChange={changeFunc} value={year}>
                <option value="">Choose Year</option>
                {filterYears.map(item => <option value={item} key={item}>{item}</option>)}
            </select>
        </div>
    )
};

export default SearchYear;
