import React from 'react';
import {FilmContext} from "../../containers/App";

const TodoListItem = ({title, poster, year, id}) => {
    return(
        <FilmContext.Consumer>
            {({handleDetailId}) => (
                <div
                    className="todo-list-item col-lg-2 col-md-4 col-sm-6 col-12 text-center"
                    onClick={() => handleDetailId(id)}
                >
                    <img src={poster} alt="No image"/>
                    <h5>{title}</h5>
                    <p>{year}</p>
                </div>
            )}
        </FilmContext.Consumer>
    )
};

export default TodoListItem;
