import React from 'react';
import {FilmContext} from "../../containers/App";
import TodoListItem from "./TodoListItem";
import EmptyZone from "../EmptyZone";

const TodoList= () => {
    const filterByYear = (year, list) => {
        if(year) {
            return list.filter(item => item.Year === year);
        }
        return list;
    };
    return(
        <FilmContext.Consumer>
            {({filmList, searchYear, searchText}) => (<div className="todo-list container-fluid">
                <div className="row">
                    {!!filmList.length && !!filterByYear(searchYear, filmList).length && filterByYear(searchYear, filmList).map(item => <TodoListItem
                        title={item.Title}
                        poster={item.Poster}
                        year={item.Year}
                        key={item.imdbID}
                        id={item.imdbID}
                    />)}
                    {!filmList.length && searchText && <EmptyZone message="There is no such films"/>}
                    {!filmList.length && !searchText && <EmptyZone message="Search some films"/>}
                    {!!filmList.length && !filterByYear(searchYear, filmList).length && <EmptyZone message="There is no such films by this year"/>}
                </div>
            </div>)}
        </FilmContext.Consumer>
    )
};

export default TodoList;
