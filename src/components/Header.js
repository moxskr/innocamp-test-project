import React from 'react';
import {FilmContext} from "../containers/App";
import Search from "./Search";
import SearchYear from "./Search/SearchYear";

const Header = () => {
    return(
        <header className="header container-fluid">
            <div className="row">
                <div className="logo col-lg-3">
                    Search Movie
                </div>
                <FilmContext.Consumer>
                    {({searchText, searchYear, handleSearch, handleSearchYear}) => {
                        return (<>
                            <Search
                                searchValue={searchText}
                                changeFunc={handleSearch}
                            />
                            <SearchYear
                                year={searchYear}
                                changeFunc={handleSearchYear}
                            />
                        </>)
                    }}
                </FilmContext.Consumer>
            </div>
        </header>
    )
};

export default Header;
