import React from 'react';

const Pagination = ({pagFunc}) => {
    return(
        <div className="container-fluid">
            <div className="row text-center">
                <div className="col-lg-12">
                    <button className="btn btn-primary" onClick={() => pagFunc(1)}>
                        1
                    </button>
                    <button className="btn btn-primary" onClick={() => pagFunc(2)}>
                        2
                    </button>
                </div>
            </div>
        </div>
    )
};

export default Pagination;
