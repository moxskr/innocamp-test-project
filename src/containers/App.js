import React from 'react';
import axios from 'axios';
import Header from "../components/Header";
import TodoList from "../components/TodoList/index";
import Details from "./Details";
import Pagination from "../components/Pagination";

export const FilmContext = React.createContext({
    filmList : [],
    searchText : '',
    searchYear : '',
    handleSearch : () => {}
});

class App extends React.Component{
    state = {
        filmList : [],
        searchText : '',
        searchYear : '',
        detailId : null,
        isDetailsOpen : false,
        page : 1,
        isLoading : false
    };
    componentDidMount() {
        this.setState({
            isLoading : true
        });
        axios.get(`http://www.omdbapi.com/?apikey=4b601aab&s=*${this.state.searchText}*&type=movie&page=${this.state.page}`)
            .then(res => {
                this.setState({
                    filmList : res.data.Search || [],
                    isLoading : false
                });
            });
    }
    handleSearch = (e) => {
        this.setState({
            searchText : e.target.value,
            page : 1,
            isLoading : true
        });
        axios.get(`http://www.omdbapi.com/?apikey=4b601aab&s=*${e.target.value}*&type=movie&page=1`)
            .then(res => {
                this.setState({
                    filmList : res.data.Search || [],
                    isLoading : false
                });
            });
    };
    handleSearchYear = e => {
        this.setState({
            searchYear : e.target.value
        });
    };
    handleDetailId = id => {
        this.setState({
            detailId : id,
            isDetailsOpen : true
        });
    };
    closeDetails = () => {
        this.setState({
            detailId : null,
            isDetailsOpen : false
        });
    };
    handlePagination = id => {
        this.setState({
            page : id,
            isLoading : true
        });
        axios.get(`http://www.omdbapi.com/?apikey=4b601aab&s=*${this.state.searchText}*&type=movie&page=${id}`)
            .then(res => {
                this.setState({
                    filmList : res.data.Search || [],
                    isLoading : false
                });
            });
    };
    render() {
        const {filmList, searchText, searchYear, detailId, isDetailsOpen, isLoading} = this.state;
        const {handleSearch, handleSearchYear, handleDetailId, closeDetails, handlePagination} = this;
        return(
            <FilmContext.Provider value={{
                filmList,
                searchText,
                searchYear,
                handleSearch,
                handleSearchYear,
                handleDetailId,
            }}>
                {isDetailsOpen && <Details detailId={detailId} closeDetails={closeDetails}/>}
                <Header/>
                {isLoading ? <div className="container text-center loading"><h4>...loading</h4></div> : <TodoList/>}
                {!!filmList.length && !searchYear && filmList.length === 10 && <Pagination pagFunc={handlePagination}/>}
            </FilmContext.Provider>
        )
    }
}

export default App;
