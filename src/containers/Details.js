import React from 'react';
import axios from 'axios';

class Details extends React.Component{
    state = {
        details : {},
        isLoading : true
    };
    componentDidMount() {
        this.setState({
            isLoading : true
        });
        axios.get("http://www.omdbapi.com/?apikey=4b601aab&i=" + this.props.detailId)
            .then((res) => {
                this.setState({
                    details : res.data,
                    isLoading : false
                });
            });
    }
    getDate = date => {
        const date_format = new Date(date);
        return date_format.getDate() + " " + (date_format.getMonth() + 1) + " " + date_format.getFullYear();
    };
    render() {
        const {
            Title,
            Director,
            Actors,
            Released,
            BoxOffice,
            Country,
            Genre,
            Runtime,
            Plot,
            imdbRating,
            imdbVotes,
            Poster
        } = this.state.details;
        return(
            <div className="details d-flex justify-content-center align-items-center">
                <div className="filter" onClick={this.props.closeDetails}></div>
                <div className="container-fluid">
                    <div className="row justify-content-center">
                        <div className="col-lg-8 details-info ">
                            <div className="details-info-header">
                                <div className="row">
                                    <div className="col-6 text-left">
                                        <h4>Details</h4>
                                    </div>
                                    <div className="col-6 text-right">
                                        <button className="btn btn-danger" onClick={this.props.closeDetails}>&times;</button>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            {!this.state.isLoading ? <div className="row">
                                <div className="col-6">
                                    <div className="details-info-main">
                                        <h5>Title : {Title}</h5>
                                        <h6>Directed by : {Director}</h6>
                                        <p>Actors : {Actors}</p>
                                        {Released !== 'N/A' ?  <p>Released : {this.getDate(Released)}</p> : <p>Released : no info</p>}
                                        {BoxOffice !== 'N/A' ? <p>Box office : {`$ ${BoxOffice}`}</p> : <p>Box office : no info</p>}
                                        <p>Country : {Country}</p>
                                        <p>Genre : {Genre}</p>
                                        {Runtime !== 'N/A' ? <p>Runtime : {Runtime}</p> : <p>Runtime : no info</p>}
                                        {Plot !== 'N/A' ? <p>Plot : {Plot}</p> : <p>Plot : no info</p>}
                                        {imdbRating !== 'N/A' && imdbVotes !== 'N/A' ?  <p>imdbRating / imdbVotes : {imdbRating} / {imdbVotes}</p> : <p>imdbRating / imdbVotes : no info</p>}
                                    </div>
                                </div>
                                {Poster !== 'N/A' && <div className="col-6">
                                    <a href={`/${Poster}`} download><img src={Poster} alt=""/></a>
                                </div>}
                            </div>
                            : <div className="details-loading"><h4>...loading</h4></div>}
                            <hr/>
                            <div className="row details-info-footer text-center">
                                <div className="col-12">
                                    <button className="btn btn-danger" onClick={this.props.closeDetails}>
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Details;
