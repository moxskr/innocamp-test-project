import React from 'react';
import {render} from 'react-dom';
import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import App from "./containers/App";

render(<App/>, document.getElementById('root'));
